import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import HomePage from './pages/home/home';
import PostDetail from './pages/detail/postDetails';
import Login from './pages/login/login';
import CreatePost from './pages/create-post/createPost';
import SignUp from './pages/signup/signup';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
      <Routes>
        <Route path='/home' element={<HomePage/>}/>
        <Route path='/home/detail/:postId' element={<PostDetail/>}/>
        <Route path='/create-post' element={<CreatePost/>}/>
        <Route exact path='/login' element = {<Login/>}/>
        <Route exact path='/sign-up' element = {<SignUp/>}/>
      </Routes>
    </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
