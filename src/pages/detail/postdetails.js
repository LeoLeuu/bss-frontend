import React, {Component, useState, useEffect} from 'react';
import TopBar from '../../components/topbar';
import styles from './styles.module.sass';
import { useParams } from "react-router-dom";
import moment from "moment";
import {Button, message, Typography } from 'antd';

const { Paragraph } = Typography;

function PostDetail(props) {

    let { postId } = useParams();
    const [post,setPost] = useState({})

    console.log(postId)

    const exportCSV = async () => {
        await fetch(`http://localhost:9000/post/exportcsv/${postId}`, {
          method: "GET",
          headers: {
            accept: "application/json",
            'Content-Type': 'application/x-download',
          },
        })
          .then((res) => {
            console.log(res)
            if (res.status == "200") {
              res.blob().then(res => {
                var fileDownload = require('js-file-download');
                fileDownload(res, `${postId}.csv`);
              })
            }
          })
          .catch((error) => console.log(error));
    };

    const getPostById = async () => {
        await fetch(`http://localhost:9000/post/${postId}`, {
          method: "GET",
          headers: {
            accept: "application/json",
          },
        })
          .then((res) => {
            if (res.status == "200") {
              res
                .json()
                .then((data) => {
                  console.log(data)
                  if (data.success === true) 
                    setPost(data.result)
                  else message.error(data.result.message)
                })
                .catch((error) => console.log(error));
            } else {
              res
                .json()
                .then((data) => {
                })
                .catch((error) => console.log(error));
            }
          })
          .catch((error) => console.log(error));
      };

    useEffect(() => {
    async function getData(){
        await getPostById()
    }
    getData()
    },[]);

    return(
        <div>
            <div>
                <TopBar/>
            </div>
            <div style={{
                width: '60%',
                margin: 'auto'
            }}>
                <div className={styles.post_item}>
                <img src={post.thumbnail}/>
                <div className={styles.post_content}>
                    <div style={{
                        textAlign: 'centre'
                    }}>
                        <text className={styles.title}>{post.title}</text>
                    </div>
                    <div className={styles.info_post}>
                        <label className={styles.author}>Author: {post.authorName}</label>
                        <label className={styles.created_on}>Created on: {moment(post.createdDate).format('YYYY-MM-DD')}</label>
                        <label className={styles.updated_on}>Updated on: {moment(post.updatedDate).format('YYYY-MM-DD')}</label>
                    </div>
                    <Paragraph ellipsis={false} style={{
                        padding: '10px'
                    }}>
                        {post.content}
                    </Paragraph>
                    <Button type='primary' onClick={exportCSV}>Export CSV</Button>
                </div>
            </div>
            </div>
        </div>
    )
}
export default PostDetail;