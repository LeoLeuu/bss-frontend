import TopBar from "../../components/topbar"
import LoginForm from "./components/loginForm"
import styles from "./styles.module.sass"

function Login(){
    const Login = details => {
        console.log(details)
    }
    return(
        <div>
            <TopBar/>
            <div className={styles.login_page}>
                <LoginForm Login={Login}/>
            </div>
        </div>
    )
}
export default Login