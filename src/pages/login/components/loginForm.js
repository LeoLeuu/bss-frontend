import { useState } from "react"
import styles from "../styles.module.sass"
import { useNavigate } from 'react-router-dom';
import { Button, message } from 'antd';

function LoginForm({Login, error}) {

    const [details, setDetails] = useState({email: "", password: ""})

    const navigate = useNavigate()

    const successMessage = () => {
        message.success('Login successfully!');
      };
    const toSignUpPage = () => {
        navigate('/sign-up', {replace: true})
    }
    const submitHandler = e => {
        e.preventDefault()

        fetch('http://localhost:9000/user/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: details.email,
                password: details.password
            })
        }).then((res) => res.json())
            .then((data) => {
                if (data.success === true){
                    localStorage.setItem('user_name',data.result.name)
                    localStorage.setItem('access_token', data.result.token)
                    successMessage()
                    console.log(data)
                    navigate('/home')
                } else if (data.success === false){
                    message.error(data.error.message)
                }
            })
            .catch ((error) => console.error(error));
    }

    return(
        <form onSubmit={submitHandler}>
            <div className={styles.form_inner}>
                <h2>Login</h2>
                <div className={styles.form_group}>
                    <label htmlFor="name">Email:</label>
                    <input type="email" required="true" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value = {details.email}/>
                </div>
                <div className={styles.form_group}>
                    <label htmlFor="password">Password:</label>
                    <input type="password" required="true" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value = {details.password}/>
                </div>
                <input type="submit" value="LOGIN"/>
                <Button type="link" onClick={toSignUpPage}>Sign up</Button>
            </div>
        </form>
    )
}
export default LoginForm