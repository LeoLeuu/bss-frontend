import TopBar from "../../components/topbar";
import 'antd/dist/antd.css';
import { Button, Form, Input, Upload, Typography, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import TextArea from "antd/lib/input/TextArea";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
function CreatePost() {
    const [form] = Form.useForm();

    const navigate = useNavigate();

    const props = {
        beforeUpload: (file) => {
          const isImage = file.type === 'image/png' || file.type === 'image/jpeg';
      
          if (!isImage) {
            message.error(`${file.name} is not a image file`);
          }
      
          return isImage || Upload.LIST_IGNORE;
        },
      };

    const formItemLayout =
        {
          labelCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        };
    const buttonItemLayout = 
        {
            wrapperCol: {
            span: 14,
            offset: 4,
            },
        }
    const normFile = (e) => {
        console.log('Upload event:', e);
        
        if (Array.isArray(e)) {
            return e;
        }
        
        return e?.fileList;
        };
    const handleInputChange = (values) => {
        console.log(values)
        fetch('http://localhost:9000/post/create', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('access_token')}`
            },
            body: JSON.stringify({
                title: values.title,
                content: values.content,
                thumbnail: values.thumbnail[0].thumbUrl
            })
        }).then((res) => res.json())
            .then((data) => {
                if (data.success === true) {
                    message.success('Create post successfully!')
                    navigate('/home')
                } else {
                    message.error(data.error.message)
                }
            })
            .catch((error) => console.log(error))
    }
    const { Title } = Typography;
    return (
        <div>
            <div>
                <TopBar/>
            </div>
            <div style={{
                width: '70%',
                margin: 'auto',
                marginTop: '50px'
            }}>
                <Form
                    name="basic"
                    {...formItemLayout}
                    onFinish={handleInputChange}
                    onFinishFailed={() => {
                        console.log("fail")
                    }}
                >
                    <Form.Item>
                        <Title style={{
                            marginLeft:'28%'
                        }}>
                            Create new Post
                        </Title>
                    </Form.Item>
                    <Form.Item 
                        label="Title"
                        name="title"
                        rules={[
                            {
                            required: true,
                            message: 'Please input title!',
                            },
                        ]}>
                        <Input placeholder="input title"/>
                    </Form.Item>
                    <Form.Item 
                        label="Content"
                        name="content"
                        rules={[
                            {
                            required: true,
                            message: 'Please input content!',
                            },
                        ]}>
                        <TextArea placeholder="input content"  style={{
                            height: '100px',

                        }}/>
                    </Form.Item>
                    <Form.Item
                        name="thumbnail"
                        label="Thumbnail"
                        valuePropName="fileList"
                        getValueFromEvent={normFile}
                        extra="example.png"
                        rules={[
                            {
                            required: true,
                            message: 'Please upload thumbnail!',
                            },
                        ]}
                    >
                        <Upload {...props} name="logo" listType="picture" >
                            <Button icon={<UploadOutlined />}>Click to upload</Button>
                        </Upload>
                    </Form.Item>
                    <Form.Item {...buttonItemLayout}>
                        <Button type="primary" htmlType="submit">Submit</Button>
                        <Button htmlType="button" onClick={() => {
                            navigate('/home')
                        }}>Cancel</Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    )
}

export default CreatePost