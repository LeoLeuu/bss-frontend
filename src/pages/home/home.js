import styles from "./styles.module.sass";
import ReactPaginate from 'react-paginate';
import React, {Component, useEffect} from "react";
import moment from "moment";
import { useState } from "react";
import PostItem from "./components/postItem";
import data from "./mockdata/mockdata.json"
import TopBar from "../../components/topbar";
import { Outlet, useNavigate } from "react-router-dom";
import { Button, Pagination } from 'antd';
import { message } from 'antd';

function HomePage() {
    const [posts, setPosts] = useState(data)
    const [page, setPage] = useState(1)
    const [total, setTotal] = useState(0)

    const pageSize = 10
    const displayPosts = posts
        .map(post => {
            return <PostItem
                title = {post.title}
                image = {post.thumbnail}
                author = {post.authorName}
                created_on = {moment(post.createdDate).format('YYYY-MM-DD')}
                updated_on = {moment(post.updatedDate).format('YYYY-MM-DD')}
                content = {post.content}
                post_id = {post.id}/>
        })

    const failMessage = () => {
      message.error('Please login to create post!');
    };

    const getPostPagination = async (pageSize, page) => {
        await fetch(`http://localhost:9000/post/pagination?pageSize=${pageSize}&page=${page}`, {
          method: "GET",
          headers: {
            accept: "application/json",
          },
        })
          .then((res) => {
            if (res.status == "200") {
              res
                .json()
                .then((data) => {
                  console.log(data)
                  setPosts(data.result.data)
                  setTotal(data.result.total)
                })
                .catch((error) => console.log(error));
            } else {
              res
                .json()
                .then((data) => {
                })
                .catch((error) => console.log(error));
            }
          })
          .catch((error) => console.log(error));
      };

    const navigate = useNavigate()
    const toCreatePost = () => {
      if (!(localStorage.getItem("access_token") === null)){
        navigate('/create-post')
      } else {
        failMessage()
      }
    }
    useEffect(() => {
      async function getData(){
        await getPostPagination(pageSize, page)
      }
      getData()
    },[]);

    const changePage = (page) => {
      console.log(page)
      setPage(page)
      async function getData(){
        await getPostPagination(pageSize, page)
      }
      getData()
    }

    // useEffect(() => {
    //   async function fetchData() {
    //     // You can await here
    //     const response = await MyAPI.getData(someId);
    //     // ...
    //   }
    //   fetchData();
    // }

    return(
        <div>
            <TopBar/>
            <div style={{
                width: '60%',
                margin: 'auto',
                marginTop: '5%'
            }}>
              <Button 
                type="primary" 
                onClick={toCreatePost}
                disabled={(localStorage.getItem("access_token") === null)}
                >Create Post</Button>
                {displayPosts}
                <Pagination 
                  style={{
                  marginLeft:'35%',
                  marginTop: '25px'
                }} defaultCurrent={1} 
                  total={total}
                  onChange={changePage} />
            </div>
      </div>
    )
}
export default HomePage;