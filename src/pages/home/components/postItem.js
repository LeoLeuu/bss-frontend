import styles from "./styles.module.sass"
import React from "react";
import  {Link}  from "react-router-dom";
import {Typography } from 'antd';

const { Paragraph } = Typography;
function PostItem(props) {
    return(
        <div className={styles.post_item}>
            <img src={props.image} alt="post"/>
            <div className={styles.post_content}>
                <div style={{
                    height: '20%',
                    textAlign: 'centre'
                }}>
                    <text className={styles.title}>{props.title}</text>
                </div>
                <div className={styles.info_post}>
                    <test className={styles.author}>Author: {props.author}</test>
                    <text className={styles.created_on}>Created on: {props.created_on}</text>
                    <text className={styles.updated_on}>Updated on: {props.updated_on}</text>
                </div>
                <Paragraph ellipsis={true} style={{
                    padding: '10px'
                }}>
                    {props.content}
                </Paragraph>
                <Link className={styles.learn_more} to={`detail/${props.post_id}`}>LEARN MORE</Link>
            </div>
        </div>
    )
}
export default PostItem