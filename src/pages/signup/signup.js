import TopBar from "../../components/topbar"
import SignUpForm from "./components/signUpForm"
import styles from "./styles.module.sass"

function SignUp(){
    const SignUp = details => {
        console.log(details)
    }
    return(
        <div>
            <TopBar/>
            <div className={styles.login_page}>
                <SignUpForm SignUp={SignUp}/>
            </div>
        </div>
    )
}
export default SignUp