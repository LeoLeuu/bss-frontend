import { useState } from "react"
import styles from "../styles.module.sass"
import { useNavigate } from 'react-router-dom';
import { Button, message } from "antd";
import validator from 'validator';

function SignUpForm({SignUp, error}) {

    const [details, setDetails] = useState({email: "", password: "", name: ""})

    const navigate = useNavigate()

    const [passwordMes, setPasswordMes] = useState('')

    const [validPassword, setValidPassword] = useState(false)

    const showError = (mes) => {
        message.error(mes);
      };

    const showSuccess = (mes) => {
        message.success(mes)
    }

    const toLoginPage = () => {
        navigate('/login', {replace: true})
    }

    const validate = (value) => {
 
        if (validator.isStrongPassword(value, {
          minLength: 8, minNumbers: 1, minSymbols: 1, minLowercase: 1, minUppercase: 1
        })){
            setPasswordMes('Password is valid')
            setValidPassword(true)
        } else {
            setPasswordMes('Password must have at least 8 characters including letters, numbers and special characters')
            setValidPassword(false)
        }
      }

    async function createUser() {
        const response = await fetch('http://localhost:9000/user/registry', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: details.email,
                password: details.password,
                name: details.name
            })
        }).then((res) => {
            if (res.status === 200) {
                fetch('http://localhost:9000/user/login', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: details.email,
                        password: details.password
                    })
                }).then((res) => res.json())
                    .then((data) => {
                        if (data.success === true){
                            localStorage.setItem('user_name',data.result.name)
                            localStorage.setItem('access_token', data.result.token)
                            console.log(data)
                            showSuccess('Sign up successfully!')
                            navigate('/home')
                        } else if (data.success === false){
                            message.error(data.error.message)
                        }
                    })
                    .catch ((error) => console.error(error));
            } else {
                res
                    .json()
                    .then((data) => {
                        showError(data.error.message)
                    })
            }
        }).catch((error) => message.error(error))
    }

    const submitHandler = e => {
        e.preventDefault()
        createUser()
    }

    return(
        <form onSubmit={submitHandler}>
            <div className={styles.form_inner}>
                <h2>Sign Up</h2>
                <div className={styles.form_group}>
                    <label htmlFor="email">Email:</label>
                    <input type="email" required='true' name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value = {details.email}/>
                </div>
                <div className={styles.form_group}>
                    <label htmlFor="password">Password:</label>
                    <input type="password" maxLength={20} required='true' name="password" id="password" 
                        onChange={e => {
                            validate(e.target.value)
                            console.log(passwordMes)
                            setDetails({...details, password: e.target.value})
                        }} value = {details.password}/>
                    {passwordMes === '' ? null :
                        <span style={{
                            fontWeight: 'bold',
                            color: 'red',
                        }}>{passwordMes}</span>}
                </div>
                <div className={styles.form_group}>
                    <label htmlFor="name">Name:</label>
                    <input type="name" required='true' name="name" id="name" onChange={e => setDetails({...details, name: e.target.value})} value = {details.name}/>
                </div>
                <input type="submit" value="SIGN UP" disabled={!validPassword}/>
                <Button type="link" onClick={toLoginPage}>Login</Button>
            </div>
        </form>
    )
}
export default SignUpForm