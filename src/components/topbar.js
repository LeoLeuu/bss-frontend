import { useNavigate } from 'react-router-dom';
import styles from './styles.module.sass'
import { DownOutlined, SmileOutlined } from '@ant-design/icons';
import { Dropdown, Menu, Space } from 'antd';
import { get } from 'react-hook-form';
import { useState } from 'react';

function TopBar() {
    const navigate = useNavigate()

    const isLogin = !(localStorage.getItem("user_name") === null)? true : false

    const userName = () => {
        if (isLogin) {
            return localStorage.getItem('user_name')
        } else return "Login"
    }
    async function remove() {
        localStorage.removeItem('access_token')
        localStorage.removeItem('user_name')
    }

    const menu = (
        <Menu
          items={[
            {
              key: '1',
              danger: true,
              disabled: !isLogin,
              label: (
                <a target="_blank" rel="noopener noreferrer" href="/home" onClick={() => {
                    remove()
                }}>
                  Log out
                </a>
              ),
            },
          ]}
        />
      );

    return(
        <div className={styles.navbar}>
            <button
            className={styles.button_home}
            onClick={() => navigate('/home')}>
                Home
            </button>
            <Dropdown overlay={menu}>
                <a style = {{
                    position: 'absolute',
                    right: '20px',
                    top:'15px'
                    }} 
                    onClick={(e) => {
                        if (!isLogin) {
                            navigate('/login')
                        }
                        e.preventDefault()
                    }}>
                <Space>
                    {userName()}
                    <DownOutlined />
                </Space>
                </a>
            </Dropdown>
        </div>
    )
}

export default TopBar;